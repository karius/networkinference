argv = commandArgs(TRUE)
netw = read.delim(argv[1])
val.set = read.delim(argv[2],header = F)


netw = netw[,c("Regulator_gene","Target_gene",argv[3])]
net.genes = unique(c(as.character(netw[,1]),as.character(netw[,2])))

val.set = val.set[which(val.set[,1] %in% net.genes & val.set[,2] %in% net.genes),]

net.pairs = paste(netw[,1],netw[,2])
val.pairs = paste(val.set[,1],val.set[,2])

prec= length(which(net.pairs %in% val.pairs))/length(net.pairs)
rec= length(which(net.pairs %in% val.pairs))/length(val.pairs)
f1 = (2*(prec*rec))/(prec+rec)

print(paste("precision: ",prec))
print(paste("recall: ",rec))
print(paste("F1 score: ",f1))
