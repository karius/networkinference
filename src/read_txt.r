argv = commandArgs(TRUE)

dir = argv[1]

files = list.files(dir)

if(length(grep("GPL",files))>0){
	files = files[-grep("GPL",files)]
}
z =0
if(dir == "Data/GSE10415_RAW/"){
	z = 4
}

data.mat = read.delim(paste(dir,files[1],sep =""),skip = z)
files = files[-1]

for(file in files){
	data = read.delim(paste(dir,file,sep =""),skip=z)
	data = data[,1:2]
	colnames(data)[2] = gsub(".txt.gz","",file)
	if(dir == "Data/GSE10415_RAW/"){
		data.mat = merge(data.mat,data,by ="ID_REF")
	}else{
		data.mat = merge(data.mat,data,by ="ID_REF")
	}
	print(file)
}

write.table(data.mat,paste("output/",gsub("/","",gsub("Data/","",dir)),"-matrix.tsv",sep = ""),row.names =F,sep = "\t",quote=F)
