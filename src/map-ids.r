
argv =commandArgs(TRUE)
netw = read.delim(argv[1])
map = read.delim(argv[2])
idtype = argv[3]
srcol= argv[4]
idtypes = c(idtype)
targetcol = argv[5]
netw[,targetcol] = gsub(" ","",netw[,targetcol])

map = map[,c("hgcn",idtype)]
map = map[-which(duplicated(map[,2])),]
map = map[-1,]

colnames(map)[2] = srcol
netw = merge(map,netw, by = srcol)

if(ncol(map)==3){
	map = map[,-2]
}
colnames(map)[2] = targetcol
netw = merge(map,netw, by= targetcol)


colnames(netw) = c("Target","Target_gene","Regulator","Regulator_gene","MI","pvalue")
print(head(netw))
print(dim(netw))
write.table(netw,"output/id-mapped-netw.tsv",quote=F,sep="\t")


