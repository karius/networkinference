library(data.table)
library(GEOquery)

go = fread("GO_term_transcription_regulation.tsv",data.table=TRUE,header=TRUE,sep="\t",fill=TRUE)
gse=getGEO(filename="training/GSE2350/data/GSE2350-GPL91_series_matrix.txt.gz")
gbs1=pData(featureData(gse))

out_table=c()
go_gene_symbols=sapply(go[,"Symbol"],tolower);
for (row in 1:nrow(gbs1)){
	gene_symbol = tolower(gbs1[row,11]);
	if (nchar(gene_symbol)>0){
		if (grepl("///",gene_symbol)){
			gene_symbols = strsplit(gene_symbol,"///")[[1]]
			in_go_gene_symbols = FALSE;
			for (symbol in gene_symbols){
				symbol=trimws(symbol);
				if (symbol %in% go_gene_symbols){
					in_go_gene_symbols = TRUE;
					break;
				}
			}
			out_table=rbind(out_table,c(gbs1[row,1],in_go_gene_symbols));
		}
		else {
			if (gene_symbol %in% go_gene_symbols){
				out_table=rbind(out_table,c(gbs1[row,1],TRUE));
			}
			else {
				out_table=rbind(out_table,c(gbs1[row,1],FALSE));
			}
		}
	} 
	else {
		out_table=rbind(out_table,c(gbs1[row,1],FALSE));
	}

}

out_table=rbind(c("ID_REF","TF"),out_table);
write.table(out_table,sep="\t",row.names=FALSE,col.names=FALSE,file="tf.tsv")
