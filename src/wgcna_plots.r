library(WGCNA)
library(reshape2)

argv = commandArgs(T)
load(argv[1])
data = read.delim(argv[2])

# module dendograms

pdf("img/wgcna-dendograms.pdf")
plotDendroAndColors(net$dendrograms[[1]], net$colors[net$blockGenes[[1]]],
"Module colors",
dendroLabels = FALSE, hang = 0.03,
addGuide = TRUE, guideHang = 0.05)

dev.off()

#Gene network
dissTOM = 1-TOMsimilarityFromExpr(t(data), power = 6)
plotTOM = dissTOM^4
diag(plotTOM) =NA

pdf("img/wgcna-netw-struct.pdf")

TOMplot(plotTOM, net$dendrograms[[1]],net$colors, main = "Network heatmap plot, all genes")
dev.off()

#Eigengene correlation between modules
MET = net$MEs
colors = net$colors
pdf("img/wgcna-eigen-cor.pdf")
plotEigengeneNetworks(MET, "Eigengene adjacency heatmap", marHeatmap = c(3,4,2,2),
plotDendrograms = FALSE, xLabelsAngle = 90)
dev.off()
