library(igraph)
argv <- commandArgs(TRUE)

#Network clustering based on topology of the network


output = read.delim(argv[1])
output = output[,c("Regulator_gene","Target_gene",argv[2] )]
output[,2] = gsub(" ","",output[,2] )
#output = output[-which(output[,2] ==""),]

weights = output$MI
#output[,1] = as.character(output[,1])
#output[,2] = as.character(output[,2])

# Create and cluster with lovain clutering
graph  = graph_from_edgelist(as.matrix(output[,1:2]), directed = F)
graph = simplify(graph)

community = cluster_louvain(graph,weights = weights)
cnames =  names(table(membership(community)))

clust.mat = c()

for(i in 1:length(cnames)){
	#write output as a file with genenames + cluster
	print(cnames)
	genes = names(which(membership(community)==cnames[i]))
	gene.mat = cbind(genes,rep(i,length(genes)))
	clust.mat = rbind(clust.mat,gene.mat)
}

colnames(clust.mat) = c("hgcn","cluster")
write.table(clust.mat,"output/output-netw-clusters.tsv",quote=F,sep ="\t",row.names =F)
