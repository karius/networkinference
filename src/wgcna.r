0#lot of this is taken from:https://horvath.genetics.ucla.edu/html/CoexpressionNetwork/Rpackages/WGCNA/Tutorials/index.html
#We could apply it on our data

library(WGCNA)
library(reshape2)

argv = commandArgs(T)
data = read.delim(argv[1])

#allows for multithreading 
enableWGCNAThreads()
powers = c(c(1:10), seq(from = 12, to=20, by=2))
# Call the network topology analysis function
sft = pickSoftThreshold(t(data), powerVector = powers, verbose = 5)

#plot to see missing indexes
plot(sft$fitIndices[,1], sign(-sft$fitIndices[,3])*sft$fitIndices[,2],
xlab="Soft Threshold (power)",ylab="Scale Free Topology Model Fit,signed R^2")

sft= sft$fitIndices
#pick the power where the plot converges
power = 6

#derive our clusters:
net = blockwiseModules(t(data), power = power,
TOMType = "unsigned", minModuleSize = 30,
reassignThreshold = 0, mergeCutHeight = 0.25,
numericLabels = TRUE, pamRespectsDendro = FALSE,
saveTOMs = FALSE,verbose = 3)

modules = table(net$colors)
clust = cbind(row.names(data),net$colors)

colnames(clust) = c("probes","cluster")
write.table(clust,"output/wgcna-clusters.tsv",quote=F,sep="\t",row.names = F)
save(net,file = "output/wgcna-results.Rdata")
#derive our network, select a module of interest
print(modules)

module = argv[2]
sub.net = data[which(rownames(data) %in% clust[which(clust[,2] == module),]),]

adjacency = adjacency(t(sub.net), power = power)
adjacency = melt(adjacency)

write.table(adjacency,"output/wgcna-adj-matrix.tsv",quote=F,sep="\t",row.names =F)
