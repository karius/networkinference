library(GEOquery)

v = commandArgs(T)

#case for just the GEO accession
if (length(v) == 1){
	geo_accession = v[1];
	#from the R documentation:"For lists, one generally uses [[ i ]] to select any single element, whereas [ i ] returns a list of the selected elements"
	#get the GEO files
	gse = getGEO(geo_accession)[[1]];
	#get the expression set, this is the numerical data
	eset = exprs(gse);
	#construct a filename
	file_name = paste(geo_accession,".tsv",sep="");
	#write to file
	write.table(eset,row.names=T,quote=F,sep="\t",file=file_name);
}

#case with a destination directory (we used it for and example) 
if (length(v) == 2){
	geo_accession = v[1];
	directory = v[2];
	#create directory and do not bitch about it if it exists
	dir.create(directory, showWarnings = F);
	gse = getGEO(geo_accession,destdir=directory)[[1]];
	eset = exprs(gse);
	file_name = paste(geo_accession,".tsv",sep="");
	write.table(eset,row.names=T,sep="\t",file=file.path(directory,file_name));	
}


