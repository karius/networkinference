library(WGCNA)
library(reshape2)

argv = commandArgs(T)
data = read.delim(argv[1],sep=",")

probes = data[,1]
map = data[,c(1,2,3)]
colnames(map) = c("probes","hgcn","entrez")
write.table(map,"Data/wgcna-id-map.tsv",quote=F,sep="\t",row.names = F)

data = data[,-c(1:8)] 
rownames(data )= probes
#filter data set with regards to too many missing values

gsg = goodSamplesGenes(t(data), verbose = 3) 
data = data[gsg$goodGenes, gsg$goodSamples]

write.table(data,"output/wgcna-data.tsv",quote=F,sep = "\t")
