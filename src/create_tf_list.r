library(data.table)
library(GEOquery)

#read in annotation file from GO
go = fread("GO_term_transcription_regulation.tsv",data.table=T,header=T,sep="\t",fill=T)
#get gene expression set 
gse=getGEO(filename="training/GSE2350/data/GSE2350-GPL91_series_matrix.txt.gz")
gbs1=pData(featureData(gse))

tf_table=c()
go_gene_symbols=sapply(go[,"Symbol"],tolower);
for (row in 1:nrow(gbs1)){
	gene_symbol = tolower(gbs1[row,11]);
	if (nchar(gene_symbol)>0){
		if (grepl("///",gene_symbol)){
			gene_symbols = strsplit(gene_symbol,"///")[[1]]
			in_go_gene_symbols = F;
			for (symbol in gene_symbols){
				symbol=trimws(symbol);
				if (symbol %in% go_gene_symbols){
					in_go_gene_symbols = T;
					break;
				}
			}
			tf_table=rbind(tf_table,c(gbs1[row,1],in_go_gene_symbols));
		}
		else {
			if (gene_symbol %in% go_gene_symbols){
				tf_table=rbind(tf_table,c(gbs1[row,1],T));
			}
			else {
				tf_table=rbind(tf_table,c(gbs1[row,1],F));
			}
		}
	} 
	else {
		tf_table=rbind(tf_table,c(gbs1[row,1],F));
	}

}

#tf_table=rbind(c("ID_REF","TF"),tf_table);
tf_list=tf_table[which(tf_table[,2]==T),1]
write.table(tf_list,sep="\t",quote=F,row.names=F,col.names=F,file="tf.txt")
