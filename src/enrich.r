library("ReactomePA")
#
#
argv = commandArgs(TRUE)
clust.tbl = read.delim(argv[1])
map = read.delim(argv[2])
map = map[,c("entrez","probes")]
#
if(length(which(is.na(map[,1])))>0){
	map = map[-which(is.na(map[,1])),]
}
#
clust.tbl = merge(map,clust.tbl,by = "probes")

clusters = names(table(clust.tbl[,3]))

res.tbl = list()
#
for(cluster in clusters){
	clust = clust.tbl[which(clust.tbl[,3] == cluster),]
	x = enrichPathway(as.character(clust.tbl$entrez), organism = "mouse",pvalueCutoff=0.05,pAdjustMethod = "BH", readable=T)
	paths = x$Description
	if(length(paths) >0){ 
		pvals = x$p.adjust
		no.genes = sapply(x$GeneRatio,function(x) strsplit(x,"/")[[1]][1])
		geneids = x$geneID

		row = cbind(paths,pvals,no.genes,geneids)
		
	}
	
	res.tbl = rbind(res.tbl,row)
}

write.table(res.tbl,"output/cluster-enrichments.tsv",quote=F,sep="\t",row.names =F)

