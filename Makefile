#Directories
Outdir = output
datadir = Data
srcdir = src

#Scripts
ARACNE = ARACNe-AP/infer.sh
celscript = ARACNe-AP/R/CEL_to_input.R
wgcnaScript = $(srcdir)/wgcna.r
clustScript = $(srcdir)/cluster.r
enrichScript = $(srcdir)/enrich.r
mapScript = $(srcdir)/map-ids.r
valScript = $(srcdir)/validation.r
wdataScript = $(srcdir)/load_wgcna_data.r
wPlots = $(srcdir)/wgcna_plots.r

#Data
gse5281 = $(Outdir)/gse5281-matrix.tsv
gse19113 = $(Outdir)/gse19113-matrix.tsv
gse17172 = $(Outdir)/gse17172-matrix.tsv
gse2350 = $(Outdir)/gse2350-matrix.tsv
network = $(Outdir)/id-mapped-netw.tsv
clusters = $(Outdir)/output-netw-clusters.tsv
idmap = $(datadir)/map-ids.txt
ids = ARACNe-AP/networks/B_Cell_network.txt
valset = $(datadir)/trrust_rawdata.human.tsv
enrezMap = $(datadir)/entrez-ids.txt
mouseliver = $(datadir)/LiverFemale3600.csv
wgcnadata = $(Outdir)/wgcna-data.tsv
wgcnaobj = $(Outdir)/wgcna-results.Rdata


gse19113: $(datadir)/GSE19113_RAW/  $(celscript) $(Outdir)
	Rscript $(celscript) $(datadir)/GSE19113_RAW/ $(Outdir)/gse19113-matrix.tsv

gse17172: $(datadir)/GSE17172_RAW/  $(celscript) $(Outdir)
	Rscript $(celscript) $(datadir)/GSE17172_RAW/ $(Outdir)/gse17172-matrix.tsv

gse5281: $(datadir)/GSE5281_RAW/  $(celscript) $(Outdir)
	Rscript $(celscript) $(datadir)/GSE5281_RAW/ $(Outdir)/gse5281-matrix.tsv

gse2350: $(datadir)/GSE2350_RAW/ $(celscript) $(Outdir)
	Rscript $(celscript)  $(datadir)/GSE2350_RAW/ $(Outdir)/gse2350-matrix.tsv

A_netw: $(ARACNE) $(gse5281) 
	./$(ARACNE) $(gse5281) 

wgcna_data: $(wdataScript) $(mouseliver)
	Rscript $(wdataScript) $(mouseliver)

wgcna_netw: $(wgcnaScript) $(gse5281)
	Rscript $(wgcnaScript) $(wgcnadata) 1

wgcna_plot: $(wPlots) $(wgcnaobj) $(wgcnadata)
	Rscript $(wPlots) $(wgcnaobj) $(wgcnadata)

cluster: $(clustScript) $(network)
	Rscript $(clustScript) $(network) MI

enrich: $(enrichScript) $(clusters)
	Rscript $(enrichScript) $(clusters) $(enrezMap)

#valid ids : "Gene.stable.ID","Transcript.stable.ID","hgu133a2","entrez","hhgu95av2","hgcn"
map: $(mapScript) $(ids) $(idmap)
	Rscript $(mapScript) $(ids) $(idmap) hhgu95av2 Regulator Target 

validation: $(valScript) $(network) $(valset) 
	Rscript $(valScript) output/id-mapped-netw.tsv $(valset) MI
	



