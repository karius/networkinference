#!/bin/bash
DIRECTORY=$(cd `dirname $0` && pwd)
origin=`pwd`
usage="Usage: $0 [ask borgthor]"
#=============================================================================
# input data
#=============================================================================
# check number of arguments
if [ ! $# -ge 1 ]
then
    echo "Argument error" 1>&2
    echo $usage 1>&2
    exit 1
fi

GENE_EXPRESSION_MATRIX=`readlink -f $1`;shift;
TF_LIST=`readlink -f $1`;shift;

while :
do
	case "$1" in 
		-n) shift; n="$1";;
		*) break;;
	esac
done

o=${o:-`pwd`}
TEMPDIR=`mktemp -d`

echo "Calculating threshold ...";
java -Xmx5G -jar $DIRECTORY/dist/aracne.jar -e "$GENE_EXPRESSION_MATRIX"  -o "$TEMPDIR"  --tfs "$TF_LIST" --pvalue 1E-8 --seed 1 --calculateThreshold
echo "Bootstrapping stuff ...";
for i in {1..100}; 
do 
	java -Xmx5G -jar $DIRECTORY/dist/aracne.jar -e "$GENE_EXPRESSION_MATRIX" -o "$TEMPDIR" --tfs "$TF_LIST" --pvalue 1E-8 --seed $i;
done
echo "Consolidating the network ...";
java -Xmx5G -jar $DIRECTORY/dist/aracne.jar -o "$TEMPDIR" --consolidate
mv "$TEMPDIR/network.txt" $origin
#rm -rf $TMPDIR
